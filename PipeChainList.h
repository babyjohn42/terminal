/*-----------------------------------------------------------------------------
	Nicholas Frederick
	PipeChainList.h
-----------------------------------------------------------------------------*/

#ifndef PIPECHAINLIST_H
#define PIPECHAINLIST_H

#include <vector>
#include <deque>
#include "CommandList.h"

/******************************************************************************
	class: PipeChainList
	members:
		_ExprList: list of expressions that a child process will attempt
			to execute, and pipe output to the process running the next
			command in the list. Vectors and char* are dynamically allocated,
			and the only commands that are allowed to accept file input/output
			redirection are the first/last command, respectively.
*******************************************************************************/
class PipeChainList
{
	public:
		~PipeChainList();

		// methods
		bool IsEmpty() {return _ExprList.empty();}
		void PopCommand();
		char** GetCommand() {return _ExprList[0]->Data();}
		void LogCommand(CommandList&);
		int Size() { return _ExprList.size();}
		void Clear() {while (!IsEmpty()){PopCommand();}}

	private:
		// members
		std::deque<CommandList*> _ExprList;
};


#endif
