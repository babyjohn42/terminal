/*-----------------------------------------------------------------------------
	Nicholas Frederick
	CommandList.cpp
-----------------------------------------------------------------------------*/

#include "CommandList.h"

/* Destroy all strings in list */
CommandList::~CommandList()
{
	this->Clear();
}

/* Destroy all strings in list */
void CommandList::Clear()
{
	for (int i=0; i<_ExprList.size(); i++)
	{
		delete[] _ExprList[i];
	}

	_ExprList.clear();
}

/* Replace the last token with a null string */
void CommandList::KeepBeforeToken()
{
	delete[] _ExprList.back();
	_ExprList.back() = nullptr;
}

/* Delete the last token in list */
void CommandList::DeleteLast()
{
	delete[] _ExprList.back();
	_ExprList.pop_back();
}
