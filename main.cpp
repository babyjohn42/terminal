/*-----------------------------------------------------------------------------
	Nicholas Frederick
	CommandList.h
-----------------------------------------------------------------------------*/

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <cstring>
#include "Terminal.h"

using namespace std;

int main(int argc, char** argv)
{
	// If there is only -c...
	if (argc==2)
	{
		cerr << "Correct input is PROG2 -c LIST" << endl;
		return -1;
	}
	
	// If there are flags...
	else if (argc>2)
	{
		// Throw error if first flag is not -c
		if(strcmp(argv[1],"-c") != 0) 
		{
			cerr << "Must use -c as first flag" << endl;
			return -1;
		} 
		
		// If command is built-in...
		if (strcmp(argv[2], "hist")==0 || strcmp(argv[2],"curPid")==0 || 
			strcmp(argv[2],"cd")==0 || strcmp(argv[2],"quit")==0)
		{
			cerr << "This is a built-in command that cannot be run with -c" << endl;
			return -1;
		}
		
		// We need a new argv with the elements of the original argv after the
		// first two, plus one nullptr element
		char* newArgv[argc-1];
		for (unsigned i=2; i<argc; i++)
		{
			newArgv[i-2] = argv[i];
		}
		newArgv[argc-2] = nullptr;
		
		// Excecute code and test for errors
		if (Terminal::ExecuteCommand(newArgv,false) < 0)
		{
			return -1;
		}

	}
	
	// There are no flags, launch terminal
	else
	{
		Terminal terminal;
		return terminal.Run();
	}
	
	return 0;
}
