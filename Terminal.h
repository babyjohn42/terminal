/*-----------------------------------------------------------------------------
	Nicholas Frederick
	Terminal.h
-----------------------------------------------------------------------------*/

#ifndef TERMINAL_H
#define TERMINAL_H

#include <deque>
#include <vector>
#include <string>
#include <sstream>
#include "PipeChainList.h"

/******************************************************************************
	class: Terminal
	members:
		_CommandHistory: list of 10 previously typed commands
		_LineNumber: the current line number in the command prompt
		_ActiveChild: the current child process we are waiting for
*******************************************************************************/
class Terminal
{	
	public:
		Terminal();
	
		// methods
		int Run();
		static void SignalHandler(int);
		static int ExecuteCommand(char**, bool = true, char* = nullptr, char* = nullptr);
		int PipeCommands(PipeChainList &, int[] = nullptr, char* = nullptr, char* = nullptr, bool = false);
	
	private:
		// members
		std::deque<std::string> _CommandHistory;
		unsigned _LineNumber;
		int _ActiveChild;
	
		// methods
		void _PrintPrompt();
		int _UserInput();
		void _IncrLineNumber(){_LineNumber++;}
		void _LogCommand(std::string&);
};


#endif
