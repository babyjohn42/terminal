/*-----------------------------------------------------------------------------
	Nicholas Frederick
	PipeChainList.cpp
-----------------------------------------------------------------------------*/

#include <vector>
#include <deque>
#include <iostream>
#include <string.h>
#include "PipeChainList.h"

using namespace std;

/* Destructor for PipeChainList */
PipeChainList::~PipeChainList()
{
	while (!_ExprList.empty())
	{
		this->PopCommand();
	}
}

/* Deallocate and delete first list of commands in _ExprList, used in recursion */
void PipeChainList::PopCommand()
{
	_ExprList[0]->Clear();
	delete _ExprList[0];
	_ExprList.pop_front();
}

/* Add list of char* to the end of _ExprList */
void PipeChainList::LogCommand(CommandList& commands)
{
	CommandList * tmpList = new CommandList;
	for (int i=0; i<commands.Size(); i++)
	{
		tmpList->AddArgument(commands[i]);
	}
	_ExprList.push_back(tmpList);
}
