# Nicholas Frederick
# makefile

CPPFLAGS=-std=c++11 -g -O3

PROG2: main.o Terminal.o PipeChainList.o CommandList.o
	    g++ main.o Terminal.o PipeChainList.o CommandList.o -o PROG2

main.o: main.cpp Terminal.h
	g++ $(CPPFLAGS) -c main.cpp

Terminal.o: Terminal.cpp Terminal.h PipeChainList.h CommandList.h
	g++ $(CPPFLAGS) -c Terminal.cpp

PipeChainList.o: PipeChainList.cpp PipeChainList.h CommandList.h
	g++ $(CPPFLAGS) -c PipeChainList.cpp

CommandList.o: CommandList.cpp CommandList.h
	g++ $(CPPFLAGS) -c CommandList.cpp

clean:
	rm -f main.o Terminal.o PipeChainList.o CommandList.o PROG2
