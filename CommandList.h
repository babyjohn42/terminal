/*-----------------------------------------------------------------------------
	Nicholas Frederick
	CommandList.h
-----------------------------------------------------------------------------*/

#ifndef COMMANDLIST_H
#define COMMANDLIST_H

#include <vector>

/******************************************************************************
	class: CommandList
	members:
		_ExprList: list of arguments making up a command to execute. The char*
			are dynamically allocated. This is essentially a class designed
			to clean up the memory when any process terminates or the list goes
			out of scope.
*******************************************************************************/
class CommandList
{
	public:
		~CommandList();
		
		// Methods
		void AddArgument(char* expr){_ExprList.push_back(expr);}
		int Size(){return _ExprList.size();}
		void KeepBeforeToken();
		void DeleteLast();
		char** Data(){return _ExprList.data();}
		void Clear();
		bool IsEmpty(){return (this->Size()==0);}
		char* operator[](int i){return _ExprList[i];}
		void EmptyList(){_ExprList.clear();}
		
	private:
		// Members
		std::vector<char*> _ExprList;
};


#endif
