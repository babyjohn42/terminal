/*-----------------------------------------------------------------------------
	Nicholas Frederick
	Terminal.cpp
-----------------------------------------------------------------------------*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <fcntl.h>
#include <sys/wait.h>
#include "Terminal.h"
#include "PipeChainList.h"
#include "CommandList.h"

using namespace std;

/* Initialize our Terminal */
Terminal::Terminal()
{
	_LineNumber = 1;
	_ActiveChild = -1;
	signal(SIGINT, SignalHandler);
	signal(SIGCHLD, SignalHandler);
}

/* Run the Terminal until there is an error or the user wants to quit. */
int Terminal::Run()
{
	int flag = 0;
	do
	{
		_PrintPrompt();      // Wait for foreground child process here
		
		flag = _UserInput(); // Only returns -1 if there is an error in a child
                             // Or if the user types 'quit'
		
	} while (flag >= 0);
		
	return 0;
}

/* Signal handler for Ctrl-C in Terminal or background child terminates */
void Terminal::SignalHandler(int n)
{
	signal(SIGINT, SignalHandler); // Do nothing with SIGINT
	signal(SIGCHLD, SignalHandler);
	if (n==SIGCHLD)
	{
		int cpid, status;
		cpid = waitpid(-1,&status,WNOHANG);
		
		// If this was a background process
		if (cpid!=0 && cpid!=-1)
		{
			// Normal termination
			if (status == 0)
			{
				cout << "[" << cpid << "] terminated" << endl;
			}
			// Abnormal termination
			else
			{
				cerr << "[" << cpid;
				psignal(WTERMSIG(status), "] termination error");
			}
		}
	}
}

/* Wait for active children and print command prompt */
void Terminal::_PrintPrompt()
{
	// Wait for child if there is one.
	if (_ActiveChild>0)
	{
		int cpid=-1;
		int status=-1;
		cpid = waitpid(_ActiveChild,&status,0);
		_ActiveChild = -1;
		if (WIFSIGNALED(status))
		{
			psignal(WTERMSIG(status), "Termination error");
		}
	}

	cout << "<" << _LineNumber << "> ";
}

// Wait for and process user input
int Terminal::_UserInput()
{
	string input, command;
	// Wait for input from user
	getline(cin, input);
	stringstream sin(input);
	CommandList commandList;
	bool isBlank = true;
	char* outname = nullptr;
	char* inname = nullptr;
	PipeChainList pipeChainList;
	
	// Process input text
	while(sin >> command)
	{	
		// We found some user input
		if (isBlank)
		{
			isBlank = false;
		}
		 
		// Copy std::string to (non-const) char*
		char* tmpString = new char[command.length()+1];
		strcpy(tmpString,command.c_str());
	
		// Add command to list of commands
		commandList.AddArgument(tmpString);
		
		// Test for built-in commands
		if (commandList.Size() == 1)
		{		
			// quit
			if (command == "quit")
			{
				if (sin >> command)
				{
					cerr << "'quit' expects no arguments" << endl;
					return 0;
				}
				else
				{
					return -1;
				}
			}

			// hist
			else if (command == "hist")
			{
				if (sin >> command)
				{
					cerr << "'hist' expects no arguments" << endl;
				}
				else
				{
					for (int i=0; i<_CommandHistory.size(); i++)
					{
						cout << _CommandHistory[i] << endl;
					}
				}
				
				// Log command in history
				_LogCommand(input);
				return 0;
			}

			// cd
			else if (command == "cd")
			{
				// Get directory
				if (sin >> command)
				{
					// If directory starts with ", include spaces
					if (command.front() == '"')
					{
						command = command.substr(1);
						
						while (command.back() != '"')
						{
							string dirAfterSpace;
							if (sin >> dirAfterSpace)
							{
								command = command + " " + dirAfterSpace;
							}
							else
							{
								cerr << "Input stream ended before ending \" found" << endl;
								
								// Log command in history
								_LogCommand(input);
								return 0;
							}
						}
						
						command.pop_back();	
					}
					
					// If there is more after the directory
					if (sin >> command)
					{
						cerr << "'cd' expects only one argument" << endl;
					}
					
					// Change current working directory to path in command
					else if (chdir(command.c_str()) < 0)
					{
						perror("cd error");
					}
				}
				else
				{
					cerr << "'cd' expects an argument" << endl;
				}
				
				// Log command in history
				_LogCommand(input);
				return 0;
			}

			// curpid
			else if (command == "curpid")
			{
				if (sin >> command)
				{
					cerr << "'curpid' expects no arguments" << endl;
				}
				else
				{
					cout << "Process ID: " << getpid() << endl;
				}
				
				// Log command in history
				_LogCommand(input);
				return 0;
			}
			
			// Token found, error!
			else if (command=="&" || command==">" || command=="<" || command=="|" || command==";")
			{
				cerr << "The tokens '<', '>', '|', '&', and ';' require a preceeding command" << endl;
				
				// Log command in history
				_LogCommand(input);
				return 0;
			}
		}
		
		// else, continue loop, assume it's an external command or token
		else
		{
			// Background process
			if (command == "&")
			{
				commandList.KeepBeforeToken();
				
				int retval;
				
				// Execute everything before '&' and continue with input processing.
				if (pipeChainList.IsEmpty())
				{
					retval = Terminal::ExecuteCommand(commandList.Data(),true,inname,outname);
					
					// Clean up memory
					commandList.Clear();
				}
				else
				{
					pipeChainList.LogCommand(commandList);
					commandList.EmptyList();
					retval = PipeCommands(pipeChainList, nullptr, inname, outname, true);
					
					// Clean up memory (pipeChainList destroys char* previously owned by commands)
					pipeChainList.Clear();
				}
				
				delete[] inname;
				delete[] outname;
				inname = nullptr;
				outname = nullptr;
				
				// Only accessed by child thread if execvp fails
				if (retval < 0)
				{
					return -1;
				}
				
				// Only accessed by parent thread
				else
				{
					cout << "[" << retval << "] in background" << endl;
				}
			}
			
			// Run command and wait before continuing
			else if (command == ";")
			{
				commandList.KeepBeforeToken();

				int retval;
				
				// Execute everything before ';' and continue with input processing.
				if (pipeChainList.IsEmpty())
				{
					retval = Terminal::ExecuteCommand(commandList.Data(),true,inname,outname);
					_ActiveChild = retval;
					
					// Clean up memory
					commandList.Clear();
				}
				
				else
				{
					pipeChainList.LogCommand(commandList);
					commandList.EmptyList();
					retval = PipeCommands(pipeChainList, nullptr, inname, outname, true);
				}
					
				delete[] inname;
				delete[] outname;
				inname = nullptr;
				outname = nullptr;
				
				// Only accessed by child thread if execvp fails
				if (retval < 0)
				{
					return -1 ;
				}
				
				// Accessed by waiting parent
				else
				{
					if (pipeChainList.IsEmpty())
					{
						int cpid=-1;
						int status=-1;
						cpid = waitpid(_ActiveChild,&status,0);
						_ActiveChild = -1;
						if (WIFSIGNALED(status))
						{
							psignal(WTERMSIG(status), "Termination error");
						}
					}
					else
					{
						pipeChainList.Clear();
					}
				}	
			}
			
			// Output redirection
			else if (command == ">")
			{
				// Check if there is a filename after '>' token
				string outFilename;
				if (sin >> outFilename)
				{	
					commandList.DeleteLast();
					
					// Set output filename. If more than '> [filename]' is in input stream,
					// the shell uses the last one and leaves the middle file empty, as
					// the Linux shell does.
					outname = new char[outFilename.length()+1];
					strcpy(outname, outFilename.c_str());
				}
				
				// If there was no filename specified after '>' token...
				else
				{
					cerr << "Must specify filename after '>' token" << endl;
					
					// Clean up memory
					delete[] inname;
					delete[] outname;
					
					_LogCommand(input);
					return 0;
				}
			}
			
			// Input redirection
			else if (command == "<")
			{
				if (pipeChainList.IsEmpty())
				{
					// Check if there is a filename after '<' token
					string inFilename;
					if (sin >> inFilename)
					{	
						commandList.DeleteLast();

						// Set input filename. If more than '< [filename]' is in input stream,
						// the shell uses the last one and leaves the middle file alone, as
						// the Linux shell does.
						inname = new char[inFilename.length()+1];
						strcpy(inname, inFilename.c_str());
					}

					// If there was no filename specified after '<' token...
					else
					{
						cerr << "Must specify filename after '<' token" << endl;

						// Clean up memory
						delete[] inname;
						delete[] outname;
						_LogCommand(input);
						return 0;
					}
				}
				
				// If there was a '|' typed before a '<'
				else
				{
					cerr << "Cannot redirect input after a pipe" << endl;
					
					// Clean up memory
					delete[] inname;
					delete[] outname;
					_LogCommand(input);
					return 0;
				}
			}
			
			// Pipe
			else if (command == "|")
			{
				commandList.KeepBeforeToken();
				
				// If there was a '>' typed before a '|'
				if (outname != nullptr)
				{
					cerr << "Cannot pipe after output redirection" << endl;
					
					// Clean up memory
					delete[] inname;
					delete[] outname;
					_LogCommand(input);
					return 0;
				}
				
				// Add everything before '|' to list of expressions to pipe together
				pipeChainList.LogCommand(commandList);
				commandList.EmptyList();
			}
		}
	}
	
	// If a command was typed...
	if (!isBlank)
	{
		// We found a & or ; token at the end of the stream
		if (commandList.IsEmpty())
		{
			_LogCommand(input);
		}
		
		// There are external commands to attempt
		else
		{	
			commandList.AddArgument(nullptr);
			int retval;

			// No pipes
			if (pipeChainList.IsEmpty())
			{
				retval = Terminal::ExecuteCommand(commandList.Data(),true,inname,outname);
				_ActiveChild = retval;
			}
			
			// If there are pipes to open
			else
			{
				pipeChainList.LogCommand(commandList);
				commandList.EmptyList();
				retval = PipeCommands(pipeChainList, nullptr, inname, outname, true);
			}
			
			// Clean up memory
			delete[] inname;
			delete[] outname;

			// Only accessed by child thread if execvp fails
			if (retval < 0)
			{
				return -1;
			}

			// Only accessed by parent thread
			else
			{
				_LogCommand(input);
			}
		}
	}
	
	return 0;
}

// Log command to command history
void Terminal::_LogCommand(string& input)
{
	stringstream sstr;
	sstr << _LineNumber << " ";
	sstr << input;
	
	// Add to command history
	_CommandHistory.push_back(sstr.str());
	
	// If already 10 in history
	if (_CommandHistory.size()>10)
	{
		_CommandHistory.pop_front();
	}
	
	_IncrLineNumber();
}

// Attempt to run single expression list
int Terminal::ExecuteCommand(char** argv, bool fromShell, char* infilename, char* outfilename)
{	
	if (fromShell)
	{
		// Try external command in a child process
		int retval;
		retval = fork();

		// Parent process
		if (retval > 0)
		{
			return retval;
		} 

		// Child process
		else
		{
			// Output redirection if '>'
			if (outfilename != nullptr)
			{
				// Open output file
				int ofd = open(outfilename, O_WRONLY | O_TRUNC | O_CREAT, S_IRWXU);
				if (ofd < 0)
				{
					perror("File open error");
					return -1;
				}
				if (dup2(ofd, 1) < 0)
				{
					perror("Dup2 error");
					return -1;
				}
			}
			
			// Input redirection if '<'
			if (infilename != nullptr)
			{
				// Open input file
				int ifd = open(infilename, O_RDONLY);
				if (ifd < 0)
				{
					perror("File open error");
					return -1;
				}
				if (dup2(ifd, 0) < 0)
				{
					perror("Dup2 error");
					return -1;
				}
			}
			
			// Excecute code and test for errors
			if (execvp(argv[0], argv) < 0)
			{
				perror("Exec error");
				return -1;
			}
		}
	}
	
	else
	{	
		// Excecute code and test for errors
		if (execvp(argv[0], argv) < 0)
		{
			perror("Exec error");
			return -1;
		}
	}
}

// Call from terminal like PipeCommands( pipeChainList, nullptr, infile, outfile, true )
// Return value of 0 from parent, -1 from failed child
int Terminal::PipeCommands(PipeChainList& pipeChainList, int fd[], char* inFile , char* outFile, bool isFirstCall)
{
	// First call to PipeCommands
	if (isFirstCall)
	{
		if (pipeChainList.Size()<2)
		{
			cout << "Must have two or more commands to use a pipe." << endl;
			return 0;
		}
		
		int retval;
		retval = fork();
		
		// Inside parent
		if (retval > 0)
		{
			int cpid = waitpid(retval, nullptr, 0);
			return 0;
		}
		
		// Inside children, begin recursive calls
		else
		{
			// Should only return -1 if there was an error
			return PipeCommands(pipeChainList, nullptr, inFile, outFile, false);
		}
				
	}
	else
	{
		// Base case in recursion
		if (pipeChainList.Size() == 1)
		{
			if (dup2(fd[0],0) < 0)
			{
				perror("Pipe input error");
				return -1;
			}
			
			// Open file output if specified
			if (outFile != nullptr)
			{
				// Open output file
				int ofd = open(outFile, O_WRONLY | O_TRUNC | O_CREAT, S_IRWXU);
				if (ofd < 0)
				{
					perror("File open error");
					return -1;
				}
				if (dup2(ofd, 1) < 0)
				{
					perror("Dup2 error");
					return -1;
				}
			}
			
			if (Terminal::ExecuteCommand(pipeChainList.GetCommand(), false) < 0)
			{
				return -1;
			}
		}
		
		// Recursive fork and pipe calls
		else
		{
			int newfd[2];
			pipe(newfd);
			
			int retval = fork();
			
			// Parent thread, execute first command in list and send output to pipe
			if (retval > 0)
			{
				// Close new pipe input
				close(newfd[0]);
				
				// Open new pipe output
				if (dup2(newfd[1],1) < 0)
				{
					perror("Pipe output error");
					return -1;
				}
				
				// Accept old pipe input if there is any
				if (fd!=nullptr)
				{	
					if (dup2(fd[0],0) < 0)
					{
						perror("Pipe input error");
						return -1;
					}
				}
				
				// If first command and inFile was passed, use it as input
				else if (inFile != nullptr)
				{
					// Open input file
					int ifd = open(inFile, O_RDONLY);
					if (ifd < 0)
					{
						perror("File open error");
						return -1;
					}
					if (dup2(ifd, 0) < 0)
					{
						perror("Dup2 error");
						return -1;
					}
				}
				
				if (Terminal::ExecuteCommand(pipeChainList.GetCommand(), false) < 0)
				{
					return -1;
				}
					
			}
			
			// Child thread, recursively call PipeCommands again until end of commands
			else
			{
				// Close new thread output
				close(newfd[1]);
				
				pipeChainList.PopCommand();
				return PipeCommands(pipeChainList, newfd, nullptr, outFile, false);
			}
		}
	}
}
